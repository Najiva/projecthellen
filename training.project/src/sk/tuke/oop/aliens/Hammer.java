package sk.tuke.oop.aliens;
import sk.tuke.oop.framework.Animation;


public class Hammer extends AbstractTool {

    public Hammer() {
        super (1);
        Animation animationHammer = new Animation("images/hammer.png", 16, 16, 10);
        animationHammer.setPingPong(true);
        setAnimation(animationHammer);
    }
    public Hammer (int use){
        super (use);
    }
}
