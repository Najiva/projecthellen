
package sk.tuke.oop.aliens;

import sk.tuke.oop.aliens.actor.AbstractActor;
import sk.tuke.oop.framework.Animation;


public class Cooler extends AbstractActor implements Switchable {
    private Reactor reactor;
    private Animation coooler;
    private boolean isRunning = false;


    public Cooler(Reactor reactor){
        this.reactor = reactor;
        coooler = new Animation("resources/images/fan.png", 32, 32, 200);
        coooler.setLooping(false);
        setAnimation(coooler);

    }
    @Override
    public void turnOn(){
        this.isRunning = true;
        updateAnimation();
    }
    @Override
    public void turnOff(){
        this.isRunning = false;
        updateAnimation();
    }

    public void updateAnimation(){
        if(this.isRunning ){
            coooler = new Animation("resources/images/fan.png", 32, 32, 200);
            coooler.setPingPong(true);
            setAnimation(coooler);
        }else{
            coooler = new Animation("resources/images/fan.png", 32, 32, 200);
            coooler.setLooping(false);
            setAnimation(coooler);
        }
    }
    @Override
    public boolean isOn(){
        if(this.isRunning ){
            return true;
        }
        else return false;
    }
    @Override
    public void act(){
        if(this.isRunning  && this.reactor != null){
            reactor.decreaseTemperature(1);
        }
    }

}