package sk.tuke.oop.aliens;


import sk.tuke.oop.aliens.actor.AbstractActor;
import sk.tuke.oop.framework.Animation;



public class Controller extends AbstractActor {
    private Reactor reactor;

    public Controller(Reactor reactor){
       Animation switchAnimation = new Animation("images/switch.png", 16, 16, 10);
        setAnimation(switchAnimation);
        this.reactor = reactor;
    }

    public void toggle() {
        if (this.reactor != null) {
            if (!this.reactor.isRunning()) {
                this.reactor.turnOn();
            } else {
                this.reactor.turnOff();
            }
        }
    }

}
