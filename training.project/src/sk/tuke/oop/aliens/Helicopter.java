package sk.tuke.oop.aliens;


import sk.tuke.oop.aliens.actor.AbstractActor;
import sk.tuke.oop.framework.Animation;

public class Helicopter extends AbstractActor {
    private boolean isDestructed;

    public Helicopter() {

        Animation helliAnimation = new Animation("resources/images/heli.png", 64 ,64, 100);
        helliAnimation.setPingPong(true);
        setAnimation(helliAnimation);
        isDestructed = false;
    }

    public void searchAndDestroy(){
        isDestructed = true;
    }

    @Override
    public void act() {
        int x = this.getX();
        int y = this.getY();
        int currentEnergy = getPlayer().getEnergy();
        if(isDestructed){
            if(getPlayer().getX() < this.getX()){
                x -= 1;
            }
            if(getPlayer().getX() > this.getX()){
                x += 1;
            }
            if(getPlayer().getY() < this.getY()){
                y -= 1;
            }
            if(getPlayer().getY() > this.getY()){
                y += 1;
            }
            this.setPosition(x,y);

            if (getPlayer().intersects(this)){
                currentEnergy -= 1;
                getPlayer().setEnergy(currentEnergy);
            }
        }
    }
}