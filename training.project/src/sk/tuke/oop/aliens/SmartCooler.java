package sk.tuke.oop.aliens;

public class SmartCooler extends Cooler implements Switchable{
    private Reactor reactor;

    public SmartCooler(Reactor reactor) {
        super(reactor);
        this.reactor = reactor;
        super.turnOff();
    }



    @Override
    public void act(){
        if (this.reactor != null){
            super.act();
        if (!super.isOn() && this.reactor.getTemperature() >2000) {
            super.turnOn();
        }
         else if (this.reactor.getTemperature() <1000 && super.isOn() ){
            super.turnOff();
        }
    }
    }
}
