package sk.tuke.oop.aliens;



import  sk.tuke.oop.framework.Animation;
import  sk.tuke.oop.aliens.actor.AbstractActor;

public class Computer extends AbstractActor implements EnergyConsumer{

    public Computer() {
        Animation normalAnimation = new Animation("images/computer.png", 80, 48, 100);
        setAnimation(normalAnimation);
    }

    public double add(double  a, double b) {
        if (a>=0 || a<=0 && b>=0 || b<=0 ) {
            double c;
            c = a + b;
            return c;
        }
        return 0;
    }

    public int add(int  a, int b) {
        if (a>=0 || a<=0 && b>=0 || b<=0 ) {
            int c;
            c = a + b;
            return c;
        }
        return 0;
    }

    public double sub(double  a, double b) {
        if (a>=0 || a<=0 && b>=0 || b<=0 ) {
            double c;
            c = a - b;
            return c;
        }
        return 0;
    }
    public int sub(int  a, int b) {
        if (a>=0 || a<=0 && b>=0 || b<=0 ) {
            int c;
            c = a - b;
            return c;
        }
        return 0;
    }
    @Override
    public void setElectricityFlow(boolean energy) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}