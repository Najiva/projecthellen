package sk.tuke.oop.aliens;

import sk.tuke.oop.framework.Animation;



public class FireExtinguisher extends AbstractTool {

    public  FireExtinguisher (){
        super(1);
        Animation extinguishedAnimation = new Animation("images/reactor_extinguished.png", 80, 80, 100);
        setAnimation(extinguishedAnimation);
    }
}
