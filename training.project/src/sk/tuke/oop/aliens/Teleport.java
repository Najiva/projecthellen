package sk.tuke.oop.aliens;
import  sk.tuke.oop.framework.Animation;
import  sk.tuke.oop.aliens.actor.AbstractActor;
import  sk.tuke.oop.aliens.actor.Player;

public class Teleport extends AbstractActor {
    private Player player;
    private Teleport telepor;
    private boolean isTeleported;

    public Teleport(Teleport teleport){
        Animation lift = new Animation("images/lift.png", 48, 48, 100);
        lift.setPingPong(true);
        setAnimation(lift);
        isTeleported = true;
        if (teleport != this.telepor){
            this.telepor = teleport;
        } else this.telepor = null;
    }


    public void setDestination(Teleport positionOfTeleport){
        if (positionOfTeleport != this){
            this.telepor = positionOfTeleport;
        }
    }
    private void teleportPlayer() {
        player = getPlayer();
        this.telepor.isTeleported = true;
        int x = this.telepor.getX() + (this.telepor.getWidth() / 2) - (player.getWidth() / 2);
        int y = this.telepor.getY() + (this.telepor.getHeight() / 2) - (player.getHeight() / 2);
        player.setPosition(x, y);
    }

    @Override
    public void act() {
        player = getPlayer();
        if (this.telepor != null && this != this.telepor) {
            if (this.isTeleported && !this.intersects(player)) {
                this.isTeleported = false;
            }
            if (!isTeleported && this.intersects(player)) {
                this.telepor.isTeleported = true;
                teleportPlayer();
            }
        }
    }
}
