package sk.tuke.oop.aliens;


public class DefectiveLight extends Light implements Repairable{
    private int time;

    public DefectiveLight(){

    }
    @Override
    public void act(){
        if(this.time != 0){
            this.time = this.time - 1;
        }
        if(Math.random() >= 0.90 && this.time == 0){
            super.toggle();
        }
    }

    @Override
    public void repairWith(AbstractTool tool){
        if(tool instanceof Wrench ) {
                this.time = 1000;
                tool.use();
        }
    }
}