package sk.tuke.oop.aliens;

import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.aliens.actor.AbstractActor;

public class PowerSwitch extends AbstractActor{
    private Switchable zariadenie;

    public PowerSwitch(Switchable zariadenie){
        this.zariadenie = zariadenie;
        Animation switcher = new Animation("resources/images/switch.png", 16, 16, 100);
        setAnimation(switcher);

    }
    public void switchOn(){
        if(this.zariadenie != null) zariadenie.turnOn();}

    public void switchOff(){
        if(this.zariadenie != null) zariadenie.turnOff(); }

}
