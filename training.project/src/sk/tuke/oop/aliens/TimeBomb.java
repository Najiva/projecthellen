package sk.tuke.oop.aliens;
import sk.tuke.oop.aliens.actor.AbstractActor;
import sk.tuke.oop.framework.Animation;

public class TimeBomb extends AbstractActor {
    private int time;
    private int a;
    private boolean isOn;
    private Animation bombactivated;
    private Animation sexplosion;

    public TimeBomb(int time){
        this.time = time;
        a = time+100;

        isOn = false;
       Animation bomb = new Animation("images/bomb.png", 16,16, 200);
        bomb.setPingPong(true);
        bombactivated = new Animation("images/bomb_activated.png", 16,16, 200);
        bombactivated.setPingPong(true);
        sexplosion = new Animation("images/small_explosion.png", 16,16, 200);
        sexplosion.setPingPong(true);
        setAnimation(bomb);
    }
    public void activate() {
        setAnimation(bombactivated);
        isOn = true;
    }


    @Override
    public void act(){
        if (isOn) {
            time--;
            a--;
            if(time == 0){
                setAnimation(sexplosion);
            }
            if (a==0){
                this.getWorld().removeActor(this);
            }
        }
    }

    public boolean isActivated(){
        return isOn;
    }
}