package sk.tuke.oop.aliens;

import sk.tuke.oop.aliens.actor.AbstractActor;

public abstract class AbstractTool extends AbstractActor { //opytat sa ci tam musi byt abstract
    private int possebleUses;

    public AbstractTool(int uses){
        this.possebleUses = uses;
    }

    public void use(){
        if(this.possebleUses > 0){
            this.possebleUses = this.possebleUses-1;
            if (this.possebleUses == 0){
                this.getWorld().removeActor(this);
            }
        }
    }
    public int getPossibleUses (){
        return possebleUses;
    }
}
