package sk.tuke.oop.aliens;

import sk.tuke.oop.aliens.actor.AbstractActor;
import sk.tuke.oop.framework.Animation;
import java.util.ArrayList;
import java.util.List;



public class Reactor extends AbstractActor implements Switchable, Repairable{
    private String manufacture;
    private int yearOfProduction;
    private int temperature;
    private int damage ;
   private boolean running;
    private Animation normalAnimation;
    private Animation hotAnimation;
    private Animation brokenAnimation;
    private Animation offAnimation;
    private EnergyConsumer device;
    private List<EnergyConsumer> devices;


    public Reactor(String manufacture) {
        temperature = 0;
        damage = 0;
        devices = new ArrayList<EnergyConsumer>();
        this.manufacture = manufacture;
        this.yearOfProduction = yearOfProduction ;
        // create animation object
        normalAnimation = new Animation("images/reactor_on.png", 80, 80, 100);
        // play animation repeatedly
        normalAnimation.setPingPong(true);
        // set actor's animation to just created Animation object

        hotAnimation = new Animation("images/reactor_hot.png", 80, 80, 50);
        hotAnimation.setPingPong(true);

        brokenAnimation = new Animation("images/reactor_broken.png", 80, 80, 50);
        brokenAnimation.setPingPong(true);

        offAnimation = new Animation("images/reactor.png", 80,80,100);
        setAnimation(normalAnimation);
        running = false;
        updateAnimation();
    }
    public int getTemperature(){
        return this.temperature;
    }

    public int getDamage (){
        return this.damage;
    }

    public boolean isRunning(){
        return running;
    }

    @Override
    public boolean isOn() {
        return isRunning();
    }

    public int getYearOfProduction(){
        return yearOfProduction;
    }
    public String getManufacture() {
        return manufacture;
    }

    public boolean isServiceNeeded () {
        if (this.temperature > 3000 && this.damage > 50){
            return true;
        } else return false;
    }

    public void increaseTemperature(int increment){
        if(isRunning() && increment >= 0){
            if(this.damage < 33){
                this.temperature += increment;
            }
            else if(this.damage >= 33 && this.damage <=66){
                this.temperature += (increment*1.5);
            }
            else{
                this.temperature +=(increment*2);
            }
            if(temperature > 2000 && this.temperature <= 6000){
                this.damage = (this.temperature - 2000) / 40;
                }
            }
        if(temperature > 6000){
            damage = 100;
            running = false;
        }
        updateAnimation();
        }




    public void decreaseTemperature(int decrement){
        if(decrement>=0 && getTemperature()>0 && running){
            if (this.damage<50){
                this.temperature -= decrement;
            }
            else if ( this.damage<100 && this.damage>=50){
                this.temperature -= (decrement/2);
            }
            else if (this.damage>=100){
                this.temperature += 0;
            }
        }
        updateAnimation();
    }



    private void  updateAnimation(){
        if (this.temperature < 4000) {
            setAnimation(normalAnimation);
        }

        if (this.temperature > 4000){
            setAnimation(hotAnimation);
        }

        if (this.temperature >= 6000){
            setAnimation(brokenAnimation);
        }
        if (!isRunning() && this.damage < 100){
            setAnimation(offAnimation);
        }
    }


@Override
    public void repairWith(AbstractTool hammer){
    if(this.damage < 100 &&  this.damage > 0 && hammer instanceof Hammer && hammer != null){
        hammer.use();
        this.damage -= 50;
        this.temperature = (this.damage*40)+2000;
    }
        if(this.damage < 0) {
            this.damage =0;
        }
        updateAnimation();
    }


@Override
public void turnOn() {
    if (this.temperature < 6000 && this.damage < 100) {
        this.running = true;
        updateAnimation();

        if (this.devices != null) {
            for (EnergyConsumer device : this.devices) {
                device.setElectricityFlow(true);
            }
        }
    }
}


    public void turnOff(){
        if(this.temperature < 6000 && this.damage < 100){
            this.running=false;
            setAnimation(offAnimation);
        }

        if (this.devices != null) {
            for (EnergyConsumer device : this.devices) {
                device.setElectricityFlow(false);
            }
        }
        if (this.damage >= 100){
            setAnimation(brokenAnimation);
        }

    }


    public void extinguishWith(AbstractTool tool) {
        if (tool instanceof FireExtinguisher){
            if (damage == 100){
                this.temperature = 4000;
                tool.use();
            }
            updateAnimation();
        }
    }



    public void addDevice (EnergyConsumer device){
    this.device = device;
    devices.add(this.device);
    this.device.setElectricityFlow(running);
}

    public void removeDevice (EnergyConsumer device){
        this.device = device;
        this.device.setElectricityFlow(false);
        devices.remove(this.device);
    }

    @Override
    public void act(){
        this.increaseTemperature(1);
    }
}



