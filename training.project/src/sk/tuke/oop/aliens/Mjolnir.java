package sk.tuke.oop.aliens;
import sk.tuke.oop.framework.Animation;


public class Mjolnir extends Hammer {
    public Mjolnir() {
        super(4);
        setAnimation(new Animation("resources/images/hammer.png",16,16,10));
    }
}
