package sk.tuke.oop.aliens;

import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.aliens.actor.AbstractActor;

public class Light extends AbstractActor implements EnergyConsumer, Switchable   {
    private boolean running;
    private boolean isPowered;
    private Animation onAnimation;
    private Animation offAnimation;


    public Light (){
        onAnimation = new Animation("images/light_on.png", 16, 16, 10);
        offAnimation = new Animation("images/light_off.png", 16, 16, 10);
        setAnimation(offAnimation);
    }

    public void toggle() {
            if (this.isPowered && !this.running) {
                this.running = true;
                setAnimation(onAnimation);
            }
            else if (!this.isPowered && !this.running){
                this.running = true;
                setAnimation(offAnimation);
            }
            else if (this.running) {
            this.running = false;
            setAnimation(offAnimation);
        }
    }


    @Override
    public void turnOn(){
        this.running= true;
        if (this.isPowered){
            setAnimation(onAnimation);
        } else setAnimation(offAnimation);
    }


    @Override
    public void turnOff(){
        this.running= false;
        setAnimation(offAnimation);
    }


    @Override
    public void setElectricityFlow(boolean powered) {
        if (powered) {
            this.isPowered = true;
            if (isPowered && running) {
                setAnimation(onAnimation);
            }
        }
        if (!powered) {
            this.isPowered = false;
            setAnimation(offAnimation);
        }
    }


    @Override
    public boolean isOn(){
        return running;
    }
}

