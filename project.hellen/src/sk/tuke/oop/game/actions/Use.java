/*
 * Michal
 */

package sk.tuke.oop.game.actions;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.game.items.Usable;
import sk.tuke.oop.game.actors.openables.Openable;

public class Use implements Action {
    private Openable openable;
    private Usable usable;
    private Actor actor;
    private Actor actuator;

    public Use(Actor actor, Actor actuator) {
        this.actuator = actuator;
        this.actor = actor;
        if (actor instanceof Openable) {
            openable = (Openable) actor;
            usable = null;
        } else if (actor instanceof Usable) {
            usable = (Usable) actor;
            openable = null;
        }
    }

    @Override
    public void execute() {
        if (usable != null) {
            usable.useBy(actuator);
        }
        if (openable != null) {
            if (openable.isOpen()) {
                //System.out.println("Closing the door.");
                openable.close();
            } else if (!openable.isOpen()) {
                //System.out.println("Opening the door.");
                openable.open();
            }
        }
        if (actuator == null && actor instanceof Usable && usable != null) {
            usable.useBy(actor);
        }

    }
}
