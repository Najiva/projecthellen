/*
 * Michal
 */

package sk.tuke.oop.game.actions;

import sk.tuke.oop.framework.ActorContainer;
import sk.tuke.oop.framework.Actor;

public class Take<A extends Actor> implements Action {
    private ActorContainer<A> container;
    private A item;

    public Take(ActorContainer<A> backpack, A actor) {
        this.container = backpack;
        this.item = actor;
    }

    @Override
    public void execute() {
        Actor actor = item;
        actor.getWorld().removeActor(item);
        this.container.add(item);
    }
}



