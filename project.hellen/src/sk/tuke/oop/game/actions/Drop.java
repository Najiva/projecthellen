/*
 * Michal
 */


package sk.tuke.oop.game.actions;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.ActorContainer;
import sk.tuke.oop.framework.World;

public class Drop<A extends Actor> implements Action {
    private int x;
    private int y;
    private ActorContainer<A> container;
    private World world;

    public Drop(ActorContainer<A> container, World world, int x, int y) {
        this.x = x;
        this.y = y;
        this.container = container;
        this.world = world;
    }

    @Override
    public void execute() {
            if(!container.getContent().isEmpty()){
                container.peek().setPosition(x, y);
                world.addActor(container.peek());
                container.remove(container.peek());
        }
    }
}