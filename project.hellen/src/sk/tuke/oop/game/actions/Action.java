/*
 * Michal
 */


package sk.tuke.oop.game.actions;

public interface Action {
    void execute();
}
