/*
 * Michal
 */

package sk.tuke.oop.game.actions;

import sk.tuke.oop.framework.ActorContainer;

public class Shift implements Action {
    private ActorContainer<?> backpack;


    public Shift(ActorContainer<?> container) {
        this.backpack = container;
    }


    @Override
    public void execute() {
        if (!backpack.getContent().isEmpty()) {
            System.out.println("Shifting intems in backpack!");
            backpack.shiftContent();
        }
    }
}
