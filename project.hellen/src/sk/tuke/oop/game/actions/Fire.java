/*
 * Michal
 */


package sk.tuke.oop.game.actions;

import sk.tuke.oop.game.actors.Armed;
import sk.tuke.oop.game.actors.weapons.Fireable;

public class Fire implements Action {
    private Armed armed;
    private Fireable shoot;

    public Fire(Armed armed) {
        this.armed = armed;
    }

    public void execute() {
        shoot = armed.getWeapon().fire();
        if (shoot != null) {
            shoot.getAnimation().setRotation(armed.getAnimation().getRotation());
            shoot.setDirection(armed.getAnimation().getRotation());
            shoot.setPosition(armed.getX(), armed.getY());
            armed.getWorld().addActor(shoot);
            shoot.addedToWorld(armed.getWorld());
        }
    }
}