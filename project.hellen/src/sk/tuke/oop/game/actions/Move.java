/*
 * Michal
 */


package sk.tuke.oop.game.actions;

import sk.tuke.oop.game.actors.Movable;
import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.game.actors.weapons.Bullet;


public class Move implements Action {
    private Movable moved;
    private int dx;
    private int dy;
    private int step;
    private int rotation;

    public void rotation(int rotation) {
        this.rotation = rotation;
    }

    public Move(Movable actor, int step, int dx, int dy) {
        moved = actor;
        this.dx = dx;
        this.dy = dy;
        this.step = step;
    }

    public void execute() {
        Actor actor = this.moved;

        actor.setPosition(actor.getX() + (dx * step), actor.getY() + (dy * step));

        if (actor.getWorld().intersectWithWall(actor) && !(actor instanceof Bullet)) {
            actor.setPosition(actor.getX() - dx * step, actor.getY() - dy * step);
        }
        if (actor.getAnimation().getRotation() != this.rotation && !(actor instanceof Bullet))
            actor.getAnimation().setRotation(this.rotation);
    }
}