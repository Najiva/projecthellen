/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Item;
import sk.tuke.oop.game.actors.AbstractActor;

public class Hammer extends AbstractActor implements Item {
    private Animation hammerAnimation;


    public Hammer(String name){
        super (name);
        hammerAnimation = new Animation("resources/sprites/hammer.png", 16, 16, 100);
        setAnimation(hammerAnimation);
        hammerAnimation.stop();
    }

    @Override
    public void act() {

    }



}
