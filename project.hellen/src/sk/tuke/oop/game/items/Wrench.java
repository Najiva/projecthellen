/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Item;
import sk.tuke.oop.game.actors.AbstractActor;

public class Wrench extends AbstractActor implements Item,Usable {
    private Animation wrenchAnimation;


    public Wrench(String name) {
        super (name);
        wrenchAnimation = new Animation("resources/sprites/wrench.png", 16, 16, 100);
        setAnimation(wrenchAnimation);
        wrenchAnimation.stop();
    }

    @Override
    public void act() {

    }

    @Override
    public void useBy(Actor actor) {

    }
}