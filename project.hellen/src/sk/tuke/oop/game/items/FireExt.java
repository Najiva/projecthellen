/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Item;
import sk.tuke.oop.game.actors.AbstractActor;

public class FireExt extends AbstractActor implements Item, Usable {

    public FireExt() {
        super("fireext");
        Animation animation = new Animation("resources/sprites/extinguisher.png", 16, 16, 100);
        setAnimation(animation);
    }

    @Override
    public void act() {
    }

    @Override
    public void useBy(Actor actor) {
    }
}