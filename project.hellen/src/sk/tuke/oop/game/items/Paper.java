/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Item;
import sk.tuke.oop.game.actors.AbstractActor;

public class Paper extends AbstractActor implements Item{
    private Animation moneyAnimation;


    public Paper(){
        super ("Paper");
        moneyAnimation = new Animation("resources/sprites/money.png", 16, 16, 100);
        setAnimation(moneyAnimation);
        moneyAnimation.stop();
    }

    @Override
    public void act() {

    }

}