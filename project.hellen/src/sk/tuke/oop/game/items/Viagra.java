/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Item;
import sk.tuke.oop.game.actors.AbstractActor;

public class Viagra extends AbstractActor implements Item, Usable {

    public Viagra(String name) {
        super(name);
        Animation animation = new Animation("resources/sprites/viagra.png", 16, 16, 100);
        setAnimation(animation);
    }

    @Override
    public void act() {

    }

    @Override
    public void useBy(Actor actor) {
        actor.setAnimation(new Animation("resources/sprites/horny.png", 32, 32, 100));
        actor.getAnimation().stop();
    }
}
