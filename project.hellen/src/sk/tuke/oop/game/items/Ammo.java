/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actors.AbstractActor;
import sk.tuke.oop.game.actors.ripley.Ripley;

public class Ammo extends AbstractActor implements Actor {
    private Animation ammo_Animation;
    private Ripley ripley;

    public Ammo(String name) {
        super(name);
        ammo_Animation = new Animation("resources/sprites/ammo.png", 16, 16, 100);
        this.setAnimation(ammo_Animation);
    }

    @Override
    public void act() {
        for (Actor actor : getWorld()) {
            if (this.intersects(actor) && actor instanceof Ripley) {
                this.ripley = (Ripley) actor;
                ripley.getWeapon().reload(50);
                getWorld().removeActor(this);
                break;
            }
        }
    }

}
