/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.ActorContainer;
import sk.tuke.oop.framework.Item;

import java.util.*;

public class Backpack implements ActorContainer<Item> {
    private int capacity;
    private Stack<Item> backpack ;
    private String name;

    public Backpack(int capacity){
        this.capacity = capacity;
        backpack = new Stack<>();
    }

    public void add(Item item) {
        System.out.println("Adding item into backpack: " + item);
        if (backpack.size() < capacity) {
            backpack.push(item);
        } else {
            System.out.println("Backpack is full.");
        }
    }

    public void remove(Item item) throws NoSuchElementException {
        if (!backpack.contains(item)) throw new  NoSuchElementException();
        else if (!backpack.empty()) {
            backpack.remove(item);
        } else {
            System.out.println("Cannot remove item, backpack is empty!");
        }
    }

    public Item peek(){
        return backpack.peek();
    }

    @Override
    public List<Item> getContent() {
        return backpack;
    }

    @Override
    public void shiftContent() {
        // Rotating stack
       Collections.rotate(backpack,1);
    }

    public int getCapacity() {
        return capacity;
    }

    public String getName(){
        return name;
    }

    public Iterator<Item> iterator() {
        return backpack.iterator();
    }

}
