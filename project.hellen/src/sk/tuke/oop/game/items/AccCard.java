/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Item;
import sk.tuke.oop.game.actors.AbstractActor;
import sk.tuke.oop.game.actors.openables.Door;

public class AccCard extends AbstractActor implements Item, Usable {
    private Animation keyAnimation;
    private Door door;

    public AccCard(){
        super ("accCard");
        keyAnimation = new Animation("resources/sprites/key.png", 16, 16, 100);
        setAnimation(keyAnimation);
    }

    @Override
    public void act() {
    }

    @Override
    public void useBy(Actor actor) {
        if(door!=null) {
            door.open();
        } else {
            System.out.println("Door is null!");
        }
    }
}
