/*
 * Michal
 */

package sk.tuke.oop.game.items;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actors.AbstractActor;
import sk.tuke.oop.game.actors.ripley.Ripley;

public class Energy extends AbstractActor implements Actor {
    private Animation energyAnimation;
    private Ripley ripley;

    public Energy(String name) {
        super(name);
        energyAnimation = new Animation("resources/sprites/energy.png", 16, 16, 100);
        setAnimation(energyAnimation);

    }


    @Override
    public void act() {
        for (Actor actor : getWorld()) {
            if (this.intersects(actor) && actor instanceof Ripley) {
                this.ripley = (Ripley) actor;
                if (ripley.getHealth().getValue() < ripley.getHealth().getmHealth()) {
                    ripley.getHealth().refill( ripley.getHealth().getmHealth());
                    getWorld().removeActor(this);
                    break;
                }
            }
        }
    }
}
