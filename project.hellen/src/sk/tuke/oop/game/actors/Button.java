/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.items.Usable;

public class Button extends AbstractActor implements Usable {
    private Animation buttonOn;
    private Animation buttonOff;
    private boolean isOn;

    public Button() {
        super("button");
        buttonOn = new Animation("resources/sprites/button_green.png", 16, 16, 100);
        buttonOff = new Animation("resources/sprites/button_red.png", 16, 16, 100);
        setAnimation(buttonOff);
        isOn = false;
    }

    @Override
    public void act() {

    }

    @Override
    public void useBy(Actor actor) {
        if (!isOn){
            setAnimation(buttonOn);
        } else {
            setAnimation(buttonOff);
        }
        isOn = !isOn;
    }

    public boolean isOn(){
        return isOn;
    }
}
