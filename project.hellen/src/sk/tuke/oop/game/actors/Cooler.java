/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.items.Usable;

public class Cooler extends AbstractActor implements Usable, Repairable{
    private Animation coolerAnimation;
    private boolean isBroke;

    public Cooler(){
        super ("cooler");
        coolerAnimation = new Animation("resources/sprites/fan.png", 32, 32, 200);
        setAnimation(coolerAnimation);
        isBroke = true;
    }

    @Override
    public void useBy(Actor actor) {

        System.out.println("Cooler was repaired");
        if (isBroke){
            isBroke = false;
            coolerAnimation.start();
        }
    }

    public boolean isBroke(){
        return isBroke;
    }

    @Override
    public void act() {
        if (isBroke) {
            coolerAnimation.stop();
        } else {
            coolerAnimation.start();
        }
    }

}
