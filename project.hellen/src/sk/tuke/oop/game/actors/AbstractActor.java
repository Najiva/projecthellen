/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.World;
import java.util.List;
import java.util.ArrayList;

public abstract class AbstractActor implements Actor {
    private int x;
    private int y;
    private String name;
    private Animation animation;
    private World world;

    protected AbstractActor (String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return name + "(" + x + ";" + y + ")";
    }

    @Override
    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    @Override
    public Animation getAnimation() {
        return animation;
    }

    @Override
    public int getHeight() {
        return animation.getHeight();
    }

    @Override
    public int getWidth() {
        return animation.getWidth();
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String getName() {
        return name;
    }

    public Actor getActorByName(String name) {
        for(Actor actor : getWorld()){
            if(actor.getName()!=null && actor.getName().contains(name)){
                return actor;
            }
        }
        return null;
    }

    @Override
    public boolean intersects(Actor actor) {
        return ((x + animation.getWidth() > actor.getX()) && (x < actor.getX() + actor.getWidth())) && ((y + animation.getHeight() > actor.getY() && y < actor.getHeight() + actor.getY()));

    }

    @Override
    public void addedToWorld(World world) {
        this.world = world;
    }

    @Override
    public World getWorld() {
        return world;
    }

    public List<Actor> getIntersectingActors() {
        List<Actor> returnList = new ArrayList<>();
        for (Actor actor : getWorld()) {
            if (this.intersects(actor)) {
                returnList.add(actor);
            }
        }

        return returnList;
    }
}


