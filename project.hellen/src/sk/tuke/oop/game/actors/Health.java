/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import java.util.List;
import java.util.ArrayList;

public class Health {
    private int health;
    private int mHealth;
    //private List<Runnable> runnableList = new ArrayList<>();
    private List<ExhaustionEffect> callbacksList = new ArrayList<ExhaustionEffect>();

    public Health(int initHealth, int maxHealth) {
        this.health = initHealth;
        this.mHealth = maxHealth;
    }

    public Health(int initHealth) {
        this.health = initHealth;
        this.mHealth = initHealth;
    }

    public int getValue() {
        return this.health;
    }

    public void refill(int amount) {
        this.health += amount;
        if (this.health > this.mHealth) {
            this.health = this.mHealth;
        }
    }

    public void restore(){
        this.health = mHealth;
    }

    public void exhaust(){
        this.health = 0;
        for (ExhaustionEffect e: callbacksList) {
            e.apply();
        }
    }

    public void drain(int amount) {
        this.health -= amount;
        if (this.health < 0) {
            this.health = 0;
            for (ExhaustionEffect e: callbacksList) {
                e.apply();
            }
        }

    }

    public int getmHealth() {
        return mHealth;
    }

    // Lepsie nazvat addCallback
    public void onExhaustion(ExhaustionEffect callback) {
        callbacksList.add(callback);
    }

    @FunctionalInterface
    public interface ExhaustionEffect{
        void apply();
    }

}

