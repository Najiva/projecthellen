/*
 * Michal
 */

package sk.tuke.oop.game.actors.ripley;

import sk.tuke.oop.framework.Input;
import sk.tuke.oop.game.actions.Fire;
import sk.tuke.oop.game.actions.Move;

public class Running implements RipleyState {
    private Ripley ripley;
    private int step = 2;

    public Running(Ripley ripley) {
        this.ripley = ripley;
    }

    @Override
    public void act() {
        Input input = Input.getInstance();

        if (input.isKeyDown(Input.Key.UP) && !(input.isKeyDown(Input.Key.LEFT)) && !(input.isKeyDown(Input.Key.RIGHT))) { //SIPKA HORE
            this.ripley.getAnimation().start();
            Move move = new Move(this.ripley, step, 0, -1);
            move.rotation(0);
            move.execute();
        } else if (input.isKeyDown(Input.Key.DOWN) && !(input.isKeyDown(Input.Key.LEFT)) && !(input.isKeyDown(Input.Key.RIGHT))) { //SIPKA DOLE
            this.ripley.getAnimation().start();
            Move move = new Move(this.ripley, step, 0, 1);
            move.rotation(180);
            move.execute();

        } else if (input.isKeyDown(Input.Key.LEFT) && !(input.isKeyDown(Input.Key.UP)) && !(input.isKeyDown(Input.Key.DOWN))) { //SIPKA DOLAVA
            this.ripley.getAnimation().start();
            Move move = new Move(this.ripley, step, -1, 0);
            move.rotation(270);
            move.execute();

        } else if (input.isKeyDown(Input.Key.RIGHT) && !(input.isKeyDown(Input.Key.UP)) && !(input.isKeyDown(Input.Key.DOWN))) { //SIPA DOPRAVA
            this.ripley.getAnimation().start();
            Move move = new Move(this.ripley, step, 1, 0);
            move.rotation(90);
            move.execute();
        } else if (input.isKeyDown(Input.Key.UP) && input.isKeyDown(Input.Key.LEFT)) {
            this.ripley.getAnimation().start();
            Move move = new Move(this.ripley, step, -1, -1);
            move.rotation(315);
            move.execute();
        } else if (input.isKeyDown(Input.Key.UP) && input.isKeyDown(Input.Key.RIGHT)) {
            this.ripley.getAnimation().start();
            Move move = new Move(this.ripley, step, 1, -1);
            move.rotation(45);
            move.execute();
        } else if (input.isKeyDown(Input.Key.DOWN) && input.isKeyDown(Input.Key.RIGHT)) {
            this.ripley.getAnimation().start();
            Move move = new Move(this.ripley, step, 1, 1);
            move.rotation(135);
            move.execute();
        } else if (input.isKeyDown(Input.Key.DOWN) && input.isKeyDown(Input.Key.LEFT)) {
            this.ripley.getAnimation().start();
            Move move = new Move(this.ripley, step, -1, 1);
            move.rotation(225);
            move.execute();
        } else this.ripley.getAnimation().stop();

        if (input.isKeyDown(Input.Key.SPACE)) {
            //System.out.println("FIREEE PUSHED!!");
            Fire fire = new Fire(this.ripley);
            fire.execute();
        }
    }
}
