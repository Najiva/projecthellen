/*
 * Michal
 */

package sk.tuke.oop.game.actors.ripley;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actors.AbstractActor;
import sk.tuke.oop.game.items.Usable;

public class Friend extends AbstractActor implements Usable {

    public Friend(String name) {
        super(name);
        Animation animation = new Animation("resources/sprites/supermario.png", 34, 71, 100);
        setAnimation(animation);
        getAnimation().stop();
    }

    @Override
    public void act() {

    }

    @Override
    public void useBy(Actor actor) {

    }
}
