/*
 * Michal
 */

package sk.tuke.oop.game.actors.ripley;

import sk.tuke.oop.framework.*;
import sk.tuke.oop.game.actions.*;
import sk.tuke.oop.game.actors.*;
import sk.tuke.oop.game.actors.enemies.Alien;
import sk.tuke.oop.game.actors.openables.*;
import sk.tuke.oop.game.actors.weapons.AbstractWeapon;
import sk.tuke.oop.game.actors.weapons.MP5;
import sk.tuke.oop.game.items.*;


public class Ripley extends AbstractActor implements Movable, Alive, Armed {

    private Animation animation;
    private Animation dieAnimation;
    private Use use;
    private Backpack backpack;
    private World world;
    private RipleyState state;
    private Running alive = new Running(this);
    private Dying dying = new Dying(this);
    private Take<Item> take;
    private Health health;
    private MP5 gun;
    private boolean won = false;
    private static Ripley instance = null;

    /**
     * @param name
     */
    protected Ripley(String name) {
        super(name);
        animation = new Animation("resources/sprites/player.png", 32, 32, 100);
        dieAnimation = new Animation("resources/sprites/player_die.png", 32, 32, 10);
        this.animation.stop();
        setPosition(0, 0);
        setAnimation(animation);
        backpack = new Backpack(9);
        state = new Running(this);
        health = new Health(500);
        gun = new MP5(300, 500);
        /*health.onExhaustion(new Health.ExhaustionEffect() {
            @Override
            public void apply() {
                state = new Dying(this);
                state.act();
            }
        });*/
    }

    public static Ripley getInstance(String name){
        if(instance == null) {
            instance = new Ripley(name);
        }
        return instance;
    }

    @Override
    public void act() {
        intersect();
        if (!won) {
            Message message = new Message("Health: " + health.getValue() + " Ammo: " + this.gun.getAmmo(), 550, 10);
            this.getWorld().showMessage(message);
        } else if (this.state.getClass() == Running.class) {
            Message message = new Message("YOU HAVE WON", 400, 300);
            this.getWorld().showMessage(message);
            this.state = new Standing(this);
            this.getAnimation().stop();
        }

        health.onExhaustion(() -> {
            state = (new Dying(this));
            state.act();
        });

        if (state.getClass() == Running.class) {
            alive.act();
        }


        Input input = Input.getInstance();

        //use
        if (input.isKeyPressed(Input.Key.E)) {
            //System.out.println("Key E pressed.");
            System.out.println("There are " + backpack.getContent().size() + " items in backpack.");
            System.out.println(this);
            // Actor is any AbstractActor

            // If we have a Viagra in our backpack we can use it and win the game
            if ((!backpack.getContent().isEmpty()) && backpack.peek().getName().equals("viagra")) {
                Item viagra = backpack.peek();
                use = new Use(viagra, this);
                System.out.println("Applying viagra!");
                use.execute();
                backpack.remove(viagra);
            }

            for (Actor actor : getWorld()) {
                //System.out.println(actor.toString());
                // If we have fire extinguisher we can extinguis hell
                if (this.intersects(actor) && actor instanceof Peklo && backpack.peek() instanceof FireExt) {
                    System.out.println("Trying to extinguis the fire with the extingusher!");
                    use = new Use(actor, this);
                    use.execute();
                    getWorld().removeActor(actor);
                    break;
                }

                // If I am opening the locker
                if(this.intersects(actor) && actor instanceof Locker && !((Locker) actor).isOpen()){
                    System.out.println("Opening the locker!");
                    use = new Use(actor, this);
                    use.execute();
                    break;
                }

                // If we are close to the not locked door
                if (this.intersects(actor) && actor instanceof Door && !(actor instanceof LockedDoor) && !(actor instanceof DoorM)) {
                    use = new Use(actor, this);
                    System.out.println("Opening the doors.");
                    use.execute();
                    break;
                }

                // We can use Computer to be stilish AF
                if (this.intersects(actor) && actor instanceof Computer) {
                    System.out.println("Computer is trying to open the door!");
                    use = new Use(actor, this);
                    use.execute();
                    break;
                }

                // Turning on the reactor
                if (this.intersects(actor) && actor instanceof Button) {
                    System.out.println("I am pushing a dangerous button!");
                    use = new Use(actor, this);
                    use.execute();
                    break;
                }

                if (!backpack.getContent().isEmpty()) {
                    // DO I HAVE WRENCH? ANYTHING CAN BE REPAIRED
                    if (this.intersects(actor) && actor instanceof Repairable && backpack.peek() instanceof Wrench && actor instanceof Reactor) {
                        use = new Use(actor, this);
                        System.out.println("Used wrench");
                        backpack.remove(backpack.peek());
                        use.execute();
                        break;
                    } else if (this.intersects(actor) && actor instanceof Openable && !(actor instanceof DoorM) && backpack.peek() instanceof AccCard) {
                        use = new Use(actor, this);
                        System.out.println("Open Door");
                        backpack.remove(backpack.peek());
                        use.execute();
                        break;
                    } else if (this.intersects(actor) && actor instanceof Usable && actor instanceof Button) {
                        use = new Use(actor, this);
                        System.out.println("Turn on reactor");
                        use.execute();
                        break;
                        // REPAIRING VENTILATOR WITH HAMMER
                    } else if (this.intersects(actor) && actor instanceof Repairable && backpack.peek() instanceof Hammer) {
                        use = new Use(actor, this);
                        System.out.println("Used hammer");
                        backpack.remove(backpack.peek());
                        use.execute();
                        // I have to turn off the fire
                        for (Actor f : getWorld()) {
                            //System.out.println("Searching for fire: " + f.toString() + " " + f.getClass());
                            if (f instanceof Peklo) {
                                //System.out.println("Found fire, I am going to remove it from world.");
                                getWorld().removeActor(f);
                                break;
                            }
                        }
                        break;

                    }
                }
            }
        }

        if (input.isKeyPressed(Input.Key.N)) {
            Shift item = new Shift(backpack);
            item.execute();
        }

        if (input.isKeyPressed(Input.Key.ENTER)) {
            //System.out.println("Key ENTER pressed.");
            for (Actor actor : getWorld()) {
                if (this.intersects(actor) && actor instanceof Item) {
                    take = new Take<>(backpack, (Item) actor);
                    if (backpack.getContent().size() < backpack.getCapacity()) {
                        take.execute();
                        break;
                    }
                }
            }
        }

        if (input.isKeyPressed(Input.Key.D)) {
            Drop<Item> item = new Drop<>(backpack, getWorld(), this.getX(), this.getY());
            item.execute();
        }

        if (input.isKeyPressed(Input.Key.ESCAPE)) {
            System.exit(0);
        }

    }


    @Override
    public void addedToWorld(World world) {
        System.out.print("Calling addedToWorld()");
        this.world = world;
        super.addedToWorld(world);
        this.world.centerOn(this);
        getWorld().showActorContainer(backpack);
    }

    @Override
    public Health getHealth() {
        return health;
    }

    @Override
    public AbstractWeapon getWeapon() {
        return gun;
    }

    @Override
    public void setWeapon(AbstractWeapon weapon) {
        this.gun = (MP5) weapon;
    }

    public Item getItemByName(String name) {
        for (Item item : backpack) {
            if (item.getName().equals(name)) {
                return item;
            }
        }
        return null;
    }

    private void intersect() {
        for (Actor actor : getWorld()) {

            if (intersects(actor) && actor instanceof Peklo) {
                health.drain(1);
            }

            if (intersects(actor) && actor instanceof Alien) {
                health.drain(1);
            }

            if (intersects(actor) && actor instanceof Tunel && ((Tunel) actor).isOpen()) {
                health.exhaust();
            }
            if (intersects(actor) && actor instanceof Mina) {
                health.exhaust();
                ((Mina) actor).setanimation();
            }

            // If you touch supermario you won.
            if (intersects(actor) && actor instanceof Friend) {
                won = true;
                actor.setAnimation(new Animation("resources/sprites/iLooveYou.png", 75, 71, 100));
                actor.getAnimation().stop();
            }

        }
    }


}
