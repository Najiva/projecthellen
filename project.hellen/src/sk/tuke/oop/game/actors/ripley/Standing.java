/*
 * Michal
 */

package sk.tuke.oop.game.actors.ripley;

public class Standing implements RipleyState {

    private Ripley ripley;

    public Standing(Ripley r) {
        this.ripley = r;
    }

    @Override
    public void act() {
    }
}
