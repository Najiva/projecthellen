/*
 * Michal
 */

package sk.tuke.oop.game.actors.ripley;

import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Input;

public class Dying implements RipleyState {
    private Animation dieAnimation;
    private Ripley ripley;

    public Dying (Ripley ripley){
        this.ripley = ripley;
        dieAnimation = new Animation("resources/sprites/player_die.png", 32, 32, 10);
        this.dieAnimation.start();
        this.dieAnimation.setPingPong(true);
        this.dieAnimation.setLooping(true);
    }

    @Override
    public void act(){
        this.ripley.setAnimation(dieAnimation);
        //this.ripley.setPosition(ripley.getX(), ripley.getY());
        //this.ripley.getAnimation().start();
    }
}
