/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actors.openables.DoorM;
import sk.tuke.oop.game.items.Usable;

public class Computer extends AbstractActor implements Usable {
    private boolean usable;

    public Computer(String name){
    super(name);
    Animation computer = new Animation("sprites/desk.png", 48, 48);
        setAnimation(computer);
        usable = false;
    }

    private boolean isOn(){
        for (Actor actor : getWorld()){
            if (actor instanceof Reactor && ((Reactor) actor).getPower()){
                    return true;
            }
        }
        return false;
    }

    @Override
    public void useBy(Actor actor) {
        if (isOn()) {
            System.out.println("Reactor is on!");
            for (Actor actor1 : getWorld()) {
                if (actor1 instanceof DoorM) {
                    ((DoorM) actor1).open();
                }
            }
        } else {
            System.out.println("Reactor is off, go and star the reactor!");
        }
    }

    @Override
    public void act() {

    }
}
