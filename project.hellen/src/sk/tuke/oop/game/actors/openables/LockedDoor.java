/*
 * Michal
 */

package sk.tuke.oop.game.actors.openables;

import sk.tuke.oop.game.items.Usable;

public class LockedDoor extends Door implements Openable{

    private boolean isLocked = true;

    public LockedDoor(String name, boolean vertical) {
        super(name, vertical);
    }

    public void lock () {
        isLocked = true;
    }
    public void isUnlocked () {
        isLocked = false;
    }
    public  boolean isLocked () {
        return  isLocked;
    }



}
