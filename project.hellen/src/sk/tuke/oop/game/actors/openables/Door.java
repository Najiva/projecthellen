/*
 * Michal
 */

package sk.tuke.oop.game.actors.openables;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actors.AbstractActor;
import sk.tuke.oop.game.actors.Observable;
import sk.tuke.oop.game.actors.Observer;
import sk.tuke.oop.game.items.Usable;

public class Door extends AbstractActor implements Openable, Observable {
    private Animation doorAnimation;
    private boolean isOpen = false;

    public Door(String name, boolean vertical) {
        super(name);
        if (vertical) {
            this.doorAnimation = new Animation("resources/sprites/vdoor.png", 16, 32, 100);
            setAnimation(doorAnimation);
            getAnimation().setPingPong(false);
            getAnimation().setLooping(false);
            getAnimation().stop();

        }

        if (!vertical) {
            this.doorAnimation = new Animation("resources/sprites/hdoor.png", 32, 16, 100);
            setAnimation(doorAnimation);
            getAnimation().setPingPong(false);
            getAnimation().setLooping(false);
            getAnimation().stop();
        }
    }


    @Override
    public void act() {
        if (!isOpen) {
            //System.out.println("Door closed " + this.getName());
            getWorld().setWall(getX() / 16, getY() / 16, true);
            //System.out.println("Placing the wall: " + this.toString() + " Wall: " + Integer.toString(getX()) + ";" + Integer.toString(getY()));
            doorAnimation.stop();
        } else {
            //System.out.println("Opened door " + this.getName());
            getWorld().setWall(getX() / 16, getY() / 16, false);
            doorAnimation.start();
        }
    }

/*    @Override
    public void useBy(Actor actor) {
        isOpen = true;
        getAnimation().setPingPong(false);
        getAnimation().setLooping(false);
        getAnimation().stop();
    }*/


    @Override
    public void open() {
        this.isOpen = true;
        getAnimation().start();
        getWorld().setWall(getX() / 16, getY() / 16, false);
    }

    @Override
    public void close() {
        this.isOpen = false;
    }

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public void addObserver(Observer observer) {

    }

    @Override
    public void removeObserver(Observer observer) {

    }
}
