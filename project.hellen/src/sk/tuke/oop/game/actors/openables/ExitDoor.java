/*
 * Michal
 */

package sk.tuke.oop.game.actors.openables;

public class ExitDoor extends Door implements Openable {

    public ExitDoor(String name, boolean vertical) {
        super(name, vertical);
    }
}
