/*
 * Michal
 */

package sk.tuke.oop.game.actors.openables;

import sk.tuke.oop.framework.Animation;

public class Tunel extends Door {
    private Animation tunel;

    public Tunel(String name, boolean vertical) {
        super(name, vertical);
        tunel = new Animation("resources/sprites/tunnel_black.png", 32, 32, 100);
        tunel.stop();
        tunel.setPingPong(false);
        tunel.setLooping(false);
        setAnimation(tunel);
    }
}
