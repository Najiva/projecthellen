/*
 * Michal
 */

package sk.tuke.oop.game.actors;
import sk.tuke.oop.framework.Animation;

public class Mina extends AbstractActor {
    private Animation bomb, explosion = new Animation("resources/sprites/small_explosion.png", 16, 16, 100);

    public Mina(String name) {
        super(name);
        this.explosion.setPingPong(true);
        this.explosion.setLooping(true);
        bomb = new Animation("resources/sprites/bomb.png", 16, 16, 100);
        setAnimation(bomb);
    }

    public void setanimation (){
        setAnimation(explosion);
    }
    @Override
    public void act() {

    }
}

