/*
 * Michal
 */

package sk.tuke.oop.game.actors;
import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.items.Usable;

public class Ventilator extends AbstractActor implements Usable, Repairable{
  private boolean repaired;
  private Animation ventilatorAnimation;

  public Ventilator(String name){
      super (name);
      ventilatorAnimation = new Animation("resources/sprites/ventilator.png", 32, 32, 100);
      setAnimation(ventilatorAnimation);
      getAnimation().stop();
      repaired = false;
  }

    @Override
    public void useBy(Actor actor) {
      System.out.println("Repairing ventilator!");
      repaired = true;
    }

    @Override
    public void act() {
      if (repaired){
          this.ventilatorAnimation.start();
      } else {
          this.ventilatorAnimation.stop();
      }
    }
}
