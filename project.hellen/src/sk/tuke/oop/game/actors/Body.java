/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Item;
import sk.tuke.oop.game.actions.Drop;
import sk.tuke.oop.game.items.*;

public class Body extends  AbstractActor implements Usable{
    private Animation bodyAnimation ;
    private Backpack backpack;
    private boolean a;

    public Body(String name){
        super(name);
        bodyAnimation = new Animation("resources/sprites/body.png", 64, 48, 100);
        setAnimation(bodyAnimation);
        backpack = new Backpack(10);
        backpack.add( new Paper());

    }

    @Override
        public void useBy(Actor actor) {
            for(int i=backpack.getContent().size(); i>0; i--){
                Drop<Item> item = new Drop<>(backpack, getWorld(), this.getX(), this.getY());
                item.execute();
            }
        }
    @Override
    public void act() {

    }
}
