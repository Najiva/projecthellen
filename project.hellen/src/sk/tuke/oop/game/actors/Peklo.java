/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actors.ripley.Ripley;

public class Peklo extends AbstractActor {

    public Peklo(String name){
        super(name);
        Animation animation = new Animation("resources/sprites/small_explosion.png",16,16,100);
        setAnimation(animation);
    }

    @Override
    public void act() {
        // I WILL KILL ANYONE WHO ENTERS ME
        for (Actor actor : getWorld()) {
            if (intersects(actor) && actor instanceof Ripley) {
                ((Ripley) actor).getHealth().exhaust();
            }
        }
    }
}

