/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.framework.Item;
import sk.tuke.oop.game.actions.Drop;
import sk.tuke.oop.game.items.Backpack;
import sk.tuke.oop.game.items.Usable;
import sk.tuke.oop.game.items.Wrench;

public class Locker extends AbstractActor implements Usable{
    private Animation lockerAnimation;
    private boolean isOpen;
    private Backpack skrinka;

    public Locker(String name) {
        super (name);
        lockerAnimation = new Animation("resources/sprites/locker.png", 16, 16, 100);
        setAnimation(lockerAnimation);
        skrinka = new Backpack(1);
        skrinka.add(new Wrench("wrench"));
        isOpen = false;
    }

    @Override
    public void act() {

    }

    @Override
    public void useBy(Actor actor) {
            Drop<Item> item = new Drop<>(skrinka, getWorld(), this.getX(), this.getY());
            item.execute();
            isOpen = true;
    }

    public boolean isOpen() {
        return isOpen;
    }

}
