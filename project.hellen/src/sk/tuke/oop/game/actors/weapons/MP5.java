/*
 * Michal
 */

package sk.tuke.oop.game.actors.weapons;

public class MP5 extends AbstractWeapon {

    public MP5(int startAmmo, int maxAmmo){
        super(startAmmo,maxAmmo);
    }

    @Override
    protected Fireable createBullet() {
        return new Bullet("bullet");
    }
}
