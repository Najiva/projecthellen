/*
 * Michal
 */

package sk.tuke.oop.game.actors.weapons;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actions.Move;
import sk.tuke.oop.game.actors.AbstractActor;
import sk.tuke.oop.game.actors.Alive;
import sk.tuke.oop.game.actors.Movable;
import sk.tuke.oop.game.actors.openables.Door;
import sk.tuke.oop.game.actors.ripley.Ripley;

public class Bullet extends AbstractActor implements Fireable {

    private int dx, dy;
    private int step;
    private int damage;



    public Bullet(String name) {
        super(name);
        setAnimation(new Animation("sprites/bullet.png", 16, 16, 100));
        step = 3;
        damage = 10;

    }

    public void setDirection(int angle) {

        if (angle == 0) {
            dx = 0;
            dy = -1;
        }
        if (angle == 90) {
            dx = +1;
            dy = 0;
        }
        if (angle == 180) {
            dx = 0;
            dy = +1;
        }
        if (angle == 270) {
            dx = -1;
            dy = 0;
        }

        if (angle == 45) {
            dx = +1;
            dy = -1;
        }
        if (angle == 135) {
            dx = +1;
            dy = +1;
        }
        if (angle == 225) {
            dx = -1;
            dy = +1;
        }
        if (angle == 315) {
            dx = -1;
            dy = -1;
        }
    }

    public void act() {

        new Move(this, step, dx, dy).execute();

        if (getWorld().intersectWithWall(this)) {
            getWorld().removeActor(this);
        }

        for (Actor actor : getWorld()) {
            if (intersects(actor) && actor instanceof Alive && !(actor instanceof Ripley)) {
                ((Alive) actor).getHealth().drain(damage);
                if (((Alive) actor).getHealth().getValue() < 1) {
                    getWorld().removeActor(actor);
                }
                getWorld().removeActor(this);
                break;
            }
            if (intersects(actor) && actor instanceof Door) {
                getWorld().removeActor(this);
                break;
            }

        }
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setStep(int step) {
        this.step = step;
    }

}