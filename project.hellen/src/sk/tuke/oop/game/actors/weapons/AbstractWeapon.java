/*
 * Michal
 */

package sk.tuke.oop.game.actors.weapons;

public abstract class AbstractWeapon {

    private int maxAmmo;
    private int currentAmmo;

    public AbstractWeapon(int startAmmo,int maxAmmo){
        this.maxAmmo=maxAmmo;
        currentAmmo=startAmmo;

    }

    public int getAmmo(){
        return currentAmmo;
    }

    public void reload(int newAmmo){
        if(currentAmmo+newAmmo>=maxAmmo){
            currentAmmo=maxAmmo;
        }else{
            currentAmmo=currentAmmo+newAmmo;
        }
    }

    public Fireable fire(){
        if(currentAmmo>=1){
            currentAmmo--;
            return createBullet();
        } else{
            return null;
        }

    }

    protected abstract Fireable createBullet();

}
