/*
 * Michal
 */

package sk.tuke.oop.game.actors;

import sk.tuke.oop.framework.*;
import sk.tuke.oop.game.actors.enemies.Alien;
import sk.tuke.oop.game.actors.enemies.SuperKiller;
import sk.tuke.oop.game.actors.openables.*;
import sk.tuke.oop.game.actors.ripley.Friend;
import sk.tuke.oop.game.actors.ripley.Ripley;
import sk.tuke.oop.game.items.*;

public class GameActorFactory implements ActorFactory {

    public GameActorFactory() {
        // TODO
    }

    @Override
    public Actor create(String type, String name) {

        // Implementuje SINGLETON
        if (name.equals("najiva")) {
            return Ripley.getInstance(name);
        }
        if (name.equals("thisDoor")) {
            return new LockedDoor("thisDoor", false);
        }
        if (name.equals("lockedh")) {
            return new LockedDoor("lockedh", true);
        }
        if (name.equals("firstDoor")) {
            return new Door("firstDoor", true);
        }
        if (name.equals("unlockeddoor") || name.equals("door2")) {
            return new Door(name, false);
        }
        if (name.equals("exitDoor")) {
            return new ExitDoor(name, true);
        }
        if (name.equals("accCard") || name.equals("card")) {
            return new AccCard();
        }
        if (name.equals("alien")) {
            return new Alien("alien");
        }
        if (name.equals("mother alien")) {
            return new Alien("mother alien");
        }
        if (name.equals("energy")) {
            return new Energy(name);
        }

        if(name.equals("locker")){
            return new Locker(name);
        }
        if (name.equals("tunel")) {
            return new Tunel(name, true);
        }
        if (name.equals("killerMachine")) {
            return new SuperKiller(name);
        }
        if (name.equals("ammo")) {
            return new Ammo(name);
        }
        if (name.equals("deadBody")) {
            return new Body(name);
        }
        if (name.equals("button")) {
            return new Button();
        }
        if (name.equals("reactor")) {
            return new Reactor();
        }
        if (name.equals("computer")) {
            return new Computer(name);
        }
        if (name.equals("doorM")) {
            return new DoorM(name, false);
        }
        if (name.equals("fireExt")) {
            return new FireExt();
        }
        if (name.equals("fire")) {
            return new Peklo(name);
        }
        if (name.equals("mina")) {
            return new Mina(name);
        }
        if (name.equals("viagra")){
            return new Viagra(name);
        }

        if(name.equals("ventilator")){
            return new Ventilator(name);
        }

        if(name.equals("supermario")){
            return new Friend(name);
        }

        if(name.equals("wrench")){
            return new Wrench(name);
        }

        if(name.equals("hammer")){
            return  new Hammer(name);
        }

        return null;
    }
}
