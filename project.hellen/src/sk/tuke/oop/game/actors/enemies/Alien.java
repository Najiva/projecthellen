/*
 * Michal
 */

package sk.tuke.oop.game.actors.enemies;

import java.util.Random;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actors.*;
import sk.tuke.oop.game.actions.Move;

public class Alien extends AbstractActor implements Movable, Enemy, Alive, Observer {
    private Animation warriorAnimation;
    private int step = 2;
    private int generatedSteps;
    private int randomInt;
    private int energy;
    private Health health;

    public Alien(String name) {
        super("alien");
        warriorAnimation = new Animation("resources/sprites/warrior_alien.png", 32, 32, 100);
        warriorAnimation.setPingPong(true);
        this.setAnimation(warriorAnimation);
        energy = 100;
        this.health = new Health(energy);
    }

    @Override
    public void act() {
        move();
        if (this.health.getValue()<=0){
            this.getWorld().removeActor(this);
        }

    }

    private void generateMovement() {
        Random random = new Random();
        this.randomInt = random.nextInt(4 + 1);
        this.generatedSteps = random.nextInt(100);
    }

    public void move() {
        if (this.generatedSteps == 0) {
            this.generateMovement();
        }
        if (randomInt == 1) { //SIPKA HORE
            this.getAnimation().start();
            Move move = new Move(this, step, 0, -1);
            move.rotation(0);
            move.execute();
        }

        if (randomInt == 2) { //SIPKA DOLE
            this.getAnimation().start();
            Move move = new Move(this, step, 0, 1);
            move.rotation(180);
            move.execute();
        }

        if (randomInt == 3) { //SIPKA DOLAVA
            Move move = new Move(this, step, 1, 0);
            move.rotation(90);
            move.execute();
        }

        if (randomInt == 4) { //SIPKA DOLAVA
            Move move = new Move(this, step, -1, 0);
            move.rotation(270);
            move.execute();
        }
        --this.generatedSteps;
    }

    @Override
    public Health getHealth() {
        return health;
    }

    @Override
    public void giveNotice() {

    }
}



