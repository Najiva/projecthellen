/*
 * Michal
 */

package sk.tuke.oop.game.actors.enemies;

import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.actors.ripley.Ripley;

public class SuperKiller extends Alien {
    private Animation killerAnimation;

    public SuperKiller(String name) {
        super(name);
        killerAnimation = new Animation("resources/sprites/spitter_alien.png", 32, 32, 200);
        setAnimation(killerAnimation);
    }

    @Override
    public void act() {
        this.move();
        for (Actor actor : getWorld()) {
            if (intersects(actor) && actor instanceof Ripley) {
                ((Ripley) actor).getHealth().exhaust();
            }
        }
    }
}
