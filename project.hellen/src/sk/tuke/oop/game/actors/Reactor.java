/*
 * Michal
 */

package sk.tuke.oop.game.actors;
import sk.tuke.oop.framework.Actor;
import sk.tuke.oop.framework.Animation;
import sk.tuke.oop.game.items.Usable;

import java.lang.annotation.Repeatable;

public class Reactor extends AbstractActor implements Usable, Repairable {
    private Animation onAnimation = new Animation("resources/sprites/reactor_on.png", 80, 80, 100);
    private Animation  offAnim = new Animation("resources/sprites/reactor.png", 80, 80, 100);
    private Animation brokenAnimation = new Animation("resources/sprites/reactor_broken.png", 80, 80, 100);
    private boolean power;
    private boolean repaired;

    public Reactor() {
        super("reactor");
        setAnimation(offAnim);
        power = false;
        repaired = false;
    }
    public boolean getPower() {
        return power;
    }

    @Override
    public void act() {
        for (Actor actor : getWorld()) {
            if (actor instanceof Button) {
                if (((Button) actor).isOn() && repaired == true) {
                    onAnimation.setLooping(true);
                    onAnimation.setPingPong(true);
                    setAnimation(onAnimation);
                    power = true;
                } else if (repaired) {
                    setAnimation(offAnim);
                } else {
                    setAnimation(brokenAnimation);
                }
            }
        }
    }



    @Override
    public void useBy(Actor actor) {
        System.out.println("Repairing reactor!!");
        repaired = true;
    }
}