package sk.tuke.oop.game;
import sk.tuke.oop.framework.SlickWorld;
import sk.tuke.oop.game.actors.*;

import java.lang.String;
public class Main{


    /**
     * The application's entry point.
     *
     * @param args the command line arguments
     */


    public static void main (String[] args) {
        SlickWorld world = new SlickWorld("Aliens", 800,600);
        GameActorFactory factory= new GameActorFactory();
        world.setFactory(factory);
        world.setMap("levels/mapOld.tmx");
        world.run();
    }

        /*
         Ripley zabije vsetkych Alienov dostane sa bud cez minove pole alebo prejde peklom aby sa dostal k jednemu exetdooru*.
         Hra sa konci ked si Ripley da viagru a zmodrie, ak chce. Hra konci ak zachrni supermaria.
         Ak sa Ripley chce dostat do pekla tak musi vojst do miestnosti kde sa nachadza reaktor, ten spusti tlacidlom na zemi. Vdakaa spustenemu reaktoru pojde pocitac ktorym si otvorime dvere
         Zdanlivo jednoduchsia moznost je prejst minove pole.*/

}
